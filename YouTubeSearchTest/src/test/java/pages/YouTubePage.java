package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class YouTubePage {

    private WebDriver driver;
    private WebDriverWait wait;

    public YouTubePage(WebDriver webDriver) {
        this.driver=webDriver;
    }

    private WebElement getAgreeButton() {return driver.findElement(By.xpath("//button[@aria-label='Agree to the use of cookies and other data for the purposes described']"));}

    private WebElement getSearchInput() {
        return driver.findElement(By.id("search"));
    }

    public WebElement getVideoSong() {
        return driver.findElement(By.xpath("//a[@id='video-title']/yt-formatted-string[@class='style-scope ytd-video-renderer']"));
    }

    private WebElement getSearchButton() {return driver.findElement(By.id("search-icon-legacy"));}

    public void acceptContent() {
        getAgreeButton().click();
    }

    public void searchForVideo() {
        getSearchInput().sendKeys(new CharSequence[]{"Cat yoga the ugly two faced"});
        getSearchButton().click();
    }

    public void openVideo() {
        getVideoSong().click();
    }
}
