package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class YouTubeVideoPage {

    private WebDriver driver;

    public YouTubeVideoPage(WebDriver webDriver) {
        this.driver=webDriver;
    }

    public WebElement getPauseButton() {
        return driver.findElement(By.xpath("//button[contains(@class,'ytp-play-button') and(@aria-label='Play (k)')]"));
    }
    public WebElement getPlayButton() {
        return driver.findElement(By.xpath("//button[contains(@class,'ytp-play-button') and(@aria-label='Pause (k)')]"));
    }
    public WebElement getFullScreenButton() {
        return driver.findElement(By.xpath("//button[@class='ytp-fullscreen-button ytp-button']"));
    }


    public void playVideo() {
        getPlayButton().click();
    }

    public void pauseVideo() {
        getPauseButton().click();
    }

    public void fullScreen() {
        getFullScreenButton().click();
    }

}

