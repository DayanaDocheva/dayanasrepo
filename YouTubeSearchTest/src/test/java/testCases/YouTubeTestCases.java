package testCases;

import resourses.Constants;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.YouTubePage;
import pages.YouTubeVideoPage;

import java.util.concurrent.TimeUnit;

public class YouTubeTestCases extends Constants {

    private WebDriver driver;

    @BeforeClass
    public static void classSetup() {
        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();
    }

    @Before
    public void setup() {
//        ChromeOptions options = new ChromeOptions();
//        options.setHeadless(true);
//        this.driver = new ChromeDriver(options);

        this.driver = new ChromeDriver();

//        FirefoxBinary firefoxBinary = new FirefoxBinary();
//        FirefoxOptions firefoxOptions = new FirefoxOptions();
//        firefoxOptions.setBinary(firefoxBinary);
//        this.driver = new FirefoxDriver(firefoxOptions);

        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        this.driver.get(YOUTUBE_HOME_PAGE);
    }

    @Test
    public void youTubeVideoStart_when_NavigatedAndClickPlayButton() {

        YouTubePage youTubePage=new YouTubePage(driver);

        youTubePage.acceptContent();
        Assert.assertEquals(YOUTUBE_PAGE_ERROR_MESSAGE, driver.getCurrentUrl(), YOUTUBE_HOME_PAGE);

        youTubePage.searchForVideo();
        Assert.assertTrue(YOUTUBE_PAGE_ERROR_MESSAGE, youTubePage.getVideoSong().isDisplayed());
        youTubePage.openVideo();

        YouTubeVideoPage youTubeVideoPage =new YouTubeVideoPage(driver);

        youTubeVideoPage.playVideo();
        Assert.assertTrue(YOUTUBE_VIDEO_IS_PAUSED_ERROR_MESSAGE, youTubeVideoPage.getPauseButton().isDisplayed());

        youTubeVideoPage.pauseVideo();
        Assert.assertTrue(YOUTUBE_VIDEO_IS_PLAYED_ERROR_MESSAGE, youTubeVideoPage.getPlayButton().isDisplayed());

        youTubeVideoPage.fullScreen();
        Assert.assertTrue(YOUTUBE_VIDEO_NOT_FULL_SCREEN_ERROR_MESSAGE, youTubeVideoPage.getFullScreenButton().isDisplayed());
    }

    @After
    public void tearDown() {
        this.driver.quit();
    }
}
