package resourses;

public class Constants {

    public static final String YOUTUBE_HOME_PAGE = "https://www.youtube.com/";
    public static final String YOUTUBE_PAGE_ERROR_MESSAGE = "Youtube main page was not displayed.";
    public static final String YOUTUBE_VIDEO_IS_PAUSED_ERROR_MESSAGE = "Video is paused.";
    public static final String YOUTUBE_VIDEO_IS_PLAYED_ERROR_MESSAGE = "Video is playing.";
    public static final String YOUTUBE_VIDEO_NOT_FULL_SCREEN_ERROR_MESSAGE = "Video is not in full screen.";

}
