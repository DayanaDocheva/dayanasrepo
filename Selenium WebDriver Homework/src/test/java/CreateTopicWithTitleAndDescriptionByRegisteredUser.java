import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;


public class CreateTopicWithTitleAndDescriptionByRegisteredUser {

    private static final String TELERIK_FORUM_HOME_PAGE = "https://stage-forum.telerikacademy.com/";
    private static final String TELERIK_FORUM_LOGIN_PAGE = "https://auth.telerikacademy.com/account/login";
    private static final String TOPIC_TITLE = "Test topic creation";
    private static final String TOPIC_BODY = "This is a test";
    private static final String EMAIL = "kravdit@gmail.com";
    private static final String PASSWORD = "buddyGroup3";
    private static final String NAVIGATE_HOME_PAGE_ERROR_MESSAGE = "Forum main page was not displayed.";
    private static final String NAVIGATE_LOGIN_PAGE_ERROR_MESSAGE = "Forum login page was not displayed.";
    private static final String LOG_IN_ERROR_MESSAGE = "The user cannot log in.";
    private static final String NEW_TOPIC_ERROR_MESSAGE = "Topic was not created.";
    private static final String LOG_OUT_ERROR_MESSAGE = "The user is logged in.";
    private WebDriver driver;

    @BeforeClass
    public static void setProperty() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Lenovo\\Drivers\\chromedriver.exe");
//      System.setProperty("webdriver.gecko.driver", "C:\\Users\\Lenovo\\Drivers\\geckodriver.exe");
    }

    @Before
    public void openBrowser() {
        driver = new ChromeDriver();
//      WebDriver driver = new FirefoxDriver();

        driver.get(TELERIK_FORUM_HOME_PAGE);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void navigateToForumHomePage() {
        Assert.assertEquals(NAVIGATE_HOME_PAGE_ERROR_MESSAGE, driver.getCurrentUrl(), TELERIK_FORUM_HOME_PAGE);
    }


    @Test
    public void navigateToLoginPage() {
        navigateToForumLoginPage();
        Assert.assertTrue( NAVIGATE_LOGIN_PAGE_ERROR_MESSAGE, driver.getCurrentUrl().contains(TELERIK_FORUM_LOGIN_PAGE));
    }

    @Test
    public void loginWhitValidCredentials() {
        navigateToForumLoginPage();
        logIn(EMAIL, PASSWORD);
        WebElement currentUser = driver.findElement(By.id("current-user"));
        Assert.assertTrue(LOG_IN_ERROR_MESSAGE, currentUser.isDisplayed());
    }


    @Test
    public void createNewTopicAfterLogIn() {
        navigateToForumLoginPage();
        logIn(EMAIL, PASSWORD);
        createNewTopic();
        Assert.assertTrue(NEW_TOPIC_ERROR_MESSAGE, driver.getPageSource().contains(TOPIC_TITLE));
    }

    @Test
    public void logOutAfterTopicIsCreated() {
        navigateToForumLoginPage();
        logIn(EMAIL, PASSWORD);
        createNewTopic();

        WebElement currentUser = driver.findElement(By.id("current-user"));
        currentUser.click();
        WebElement preferences = driver.findElement(By.className("user-preferences-link"));
        preferences.click();
        WebElement logOutButton = driver.findElement(By.className("d-button-label"));
        logOutButton.click();

        WebElement loginButton = driver.findElement(By.className("d-button-label"));
        Assert.assertTrue(LOG_OUT_ERROR_MESSAGE, loginButton.isDisplayed());
    }

    @After
    public void closeBrowser() {
        driver.close();
    }

    private void navigateToForumLoginPage() {
        WebElement loginButton = driver.findElement(By.className("d-button-label"));
        loginButton.click();
    }

    private void logIn(String email, String password) {
        WebElement emailField = driver.findElement(By.id("Email"));
        emailField.click();
        emailField.sendKeys(email);
        WebElement passwordField = driver.findElement(By.id("Password"));
        passwordField.sendKeys(password);
        WebElement signInButton = driver.findElement(By.id("next"));
        signInButton.click();
    }

    private void createNewTopic() {
        WebElement newTopicButton = driver.findElement(By.id("create-topic"));
        newTopicButton.click();
        WebElement titleField = driver.findElement(By.id("reply-title"));
        titleField.sendKeys(TOPIC_TITLE);
        WebElement bodyField = driver.findElement(By.xpath("//textarea[@aria-label='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.'] "));
        bodyField.sendKeys(TOPIC_BODY);
        WebElement createTopicButton = driver.findElement(By.xpath("//button[@aria-label='Create Topic']"));
        createTopicButton.click();
    }
}
