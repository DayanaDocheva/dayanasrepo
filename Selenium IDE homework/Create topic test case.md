Title: 
 - Create topic with title and description by registered user

    
Narrative:
 - In order to create a new topic

 - As a member of the Telerik forum

 - I want to create topic a new topic with title and description


Steps to reproduce:

 1. Open Chrome browser.
 2. Go to https://stage-forum.telerikacademy.com/.
 3. Log in with member account.
 4. Click "New Topic" button.
 5. Fill in the title field.
 6. Fill in the body field.
 7. Click "Create Topic" button.
 8. Navigate to the top right hand corner
 9. Click on the header dropdown tongle current user.
 10. Click "preferences icon".
 11. Click "log out" button.


Expected result:

 - A new topic is created.