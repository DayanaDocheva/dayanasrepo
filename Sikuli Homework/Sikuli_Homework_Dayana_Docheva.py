##Creation of topic being a registered user

log_in ="log_in.png"
email_field = "email_field.png"
password_field = "password_field.png"
sign_in = "sign_in.png"
new_topic_button = "new_topic_button.png"
title_field = "title_field.png"
topic_field = "topic_field.png"
create_topic_button = "create_topic_button.png"
created_topic = "created_topic.png"
user_icon = "user_icon.png"
preferences_icon = "preferences_icon.png"
log_out = "log_out.png"


type("r", KeyModifier.META)	
sleep(2)
type("chrome /incognito")	
sleep(2)
type(Key.ENTER)
sleep(1)	
type("https://stage-forum.telerikacademy.com/")
type(Key.ENTER)
wait(2)

click(log_in)
wait(2)
click(email_field)
wait(2)
type("kravdit@gmail.com")
wait(2)
click(password_field)
wait(2)
type("buddyGroup3")       
wait(2)
click(sign_in)
wait(3)
click(new_topic_button)
wait(2)
type("Topic creation test")
wait(2)
click(topic_field)
wait(2)
type("Testing creation of topic in the forum")
click(create_topic_button)
##The automated test should have assertion - check what you created properly.
assert exists(created_topic, 3)

click(user_icon)
wait(2)
click(preferences_icon)
wait(2)
click(log_out)
wait(2)
type(Key.F4, KeyModifier.ALT)

