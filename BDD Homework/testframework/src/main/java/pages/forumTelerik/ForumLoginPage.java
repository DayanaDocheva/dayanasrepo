package pages.forumTelerik;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class ForumLoginPage extends BasePage {

    public ForumLoginPage(WebDriver driver) {
        super(driver, "forum.loginPageURL");
    }

    public void logIn(String email, String password) {
        actions.typeValueInField(email, "forum.loginPage.emailField");
        actions.typeValueInField(password, "forum.loginPage.passwordField");
        actions.clickElement("forum.loginPage.signInButton");
    }

    public void assertUserIsLoggedIn() {
        actions.waitForElementVisibleUntilTimeout("forum.homePage.currentUserIcon", 10);
        actions.assertElementPresent("forum.homePage.currentUserIcon");
    }


}
