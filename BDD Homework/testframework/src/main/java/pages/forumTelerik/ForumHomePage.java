package pages.forumTelerik;


import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;


public class ForumHomePage extends BasePage {


    public ForumHomePage(WebDriver driver) {
        super(driver, "forum.homePageURL");
    }


    public void navigateToForumLoginPage() {
        actions.waitForElementVisibleUntilTimeout("forum.homePage.logInButton", 10);
        actions.clickElement("forum.homePage.logInButton");
    }

    public void createNewTopic(String topicTitle, String topicBody) {
        actions.waitForElementVisibleUntilTimeout("forum.homePage.newTopicButton", 10);
        actions.clickElement("forum.homePage.newTopicButton");
        actions.typeValueInField(topicTitle, "forum.homePage.titleField");
        actions.typeValueInField(topicBody, "forum.homePage.bodyField");
        actions.waitForElementVisibleUntilTimeout("forum.homePage.createTopicButton", 10);
        actions.clickElement("forum.homePage.createTopicButton");
    }


    public void logOut() {
        actions.waitForElementVisibleUntilTimeout("forum.homePage.currentUserIcon", 10);
        actions.clickElement("forum.homePage.currentUserIcon");
        actions.waitForElementVisibleUntilTimeout("forum.homePage.preferencesIcon", 10);
        actions.clickElement("forum.homePage.preferencesIcon");
        actions.waitForElementVisibleUntilTimeout("forum.homePage.logOutButton", 10);
        actions.clickElement("forum.homePage.logOutButton");
    }

    public void assertHomePageNavigated() {
        actions.assertElementPresent("forum.homePageURL");
    }

    public void assertLoginPageNavigated() {
        actions.assertElementPresent("forum.loginPage.siteLogo");
    }

    public void assertCreateNewTopic() {
        actions.waitForElementVisibleUntilTimeout("forum.homePage.replyButton", 10);
        actions.assertElementPresent("forum.homePage.replyButton");
    }

    public void assertUserIsLoggedOut() {
        actions.waitForElementVisibleUntilTimeout("forum.homePage.logInButton", 10);
        actions.assertElementPresent("forum.homePage.logInButton");

    }
}
