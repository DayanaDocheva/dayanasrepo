package stepDefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.BeforeStory;
import pages.forumTelerik.ForumHomePage;
import pages.forumTelerik.ForumLoginPage;

public class BaseStepDefinitions {


    UserActions actions = new UserActions();
    ForumHomePage homePage = new ForumHomePage(actions.getDriver());
    ForumLoginPage loginPage = new ForumLoginPage(actions.getDriver());


    @BeforeStory
    public static void setUp() {
        UserActions.loadBrowser("forum.homePageURL");
    }

    @BeforeScenario
    public void setInstances() {
        UserActions actions = new UserActions();
        ForumHomePage homePage = new ForumHomePage(actions.getDriver());
        ForumLoginPage loginPage = new ForumLoginPage(actions.getDriver());
    }

    @AfterStory
    public static void tearDown() {
        UserActions.quitDriver();
    }

}
