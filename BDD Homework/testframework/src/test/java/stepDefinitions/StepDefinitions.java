package stepDefinitions;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;


public class StepDefinitions extends BaseStepDefinitions {

    private static final String EMAIL = "kravdit@gmail.com";
    private static final String PASSWORD = "buddyGroup3";
    private static final String TOPIC_TITLE = "Test is a new topic";
    private static final String TOPIC_BODY = "This is a test";



    @Given("The user is logged in with valid credentials")
    public  void LogIn() {
        homePage.navigateToForumLoginPage();
        homePage.assertLoginPageNavigated();

        loginPage.logIn(EMAIL, PASSWORD);
        loginPage.assertUserIsLoggedIn();
    }

    @When("The user create a new topic with title $title and description $description in the forum")
    public void createTopic() {
        homePage.createNewTopic(TOPIC_TITLE, TOPIC_BODY);
    }

    @Then("New topic is displayed")
    public void topicDisplayed() {
        homePage.assertCreateNewTopic();
    }


    @Given("New topic is displayed")
    public void topicDisplayed2() {
        homePage.assertCreateNewTopic();
    }

    @When("The user is logged out")
    public void logOut() {
        homePage.logOut();
    }

    @Then("Log in button is displayed")
    public void logInButtonDisplayed() {
        homePage.assertUserIsLoggedOut();
    }


}
