package testCases.forumTelerik;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.forumTelerik.ForumHomePage;
import pages.forumTelerik.ForumLoginPage;

public class TelerikForumTopicCreationTests extends BaseTest {

    private static final String TOPIC_TITLE = "Test topic creation";
    private static final String TOPIC_BODY = "This is a test";
    private static final String EMAIL = "kravdit@gmail.com";
    private static final String PASSWORD = "buddyGroup3";


    @Test
    public void createNewTopicAndLogOut_When_userIsLoggedIn() {
        ForumHomePage homePage = new ForumHomePage(actions.getDriver());
        homePage.navigateToForumLoginPage();
        homePage.assertLoginPageNavigated();

        ForumLoginPage loginPage = new ForumLoginPage(actions.getDriver());
        loginPage.logIn(EMAIL, PASSWORD);
        loginPage.assertUserIsLoggedIn();

        homePage.createNewTopic(TOPIC_TITLE, TOPIC_BODY);
        homePage.assertCreateNewTopic();

        homePage.logOut();
        homePage.assertUserIsLoggedOut();

    }
}
