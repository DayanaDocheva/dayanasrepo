Meta:
@createTopic

Narrative:
As a member of Telerik forum
I want to create a new topic with title and description
So that I can discuss different things with my colleagues in the forum

Scenario: Create new topic with title and description

Given The user is logged in with valid credentials
When The user create a new topic with title This is a new topic and description This is a test in the forum
Then New topic is displayed

Scenario: Log out after topic is created

Given New topic is displayed
When The user is logged out
Then Log in button is displayed