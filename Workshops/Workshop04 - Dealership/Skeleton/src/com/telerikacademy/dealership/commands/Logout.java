package com.telerikacademy.dealership.commands;

import com.telerikacademy.dealership.commands.contracts.Command;
import com.telerikacademy.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.dealership.core.contracts.DealershipRepository;

import java.util.List;

import static com.telerikacademy.dealership.commands.constants.CommandConstants.USER_LOGGED_OUT;

public class Logout implements Command {
    
    private final DealershipFactory dealershipFactory;
    private final DealershipRepository dealershipRepository;
    
    
    public Logout(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }
    
    @Override
    public String execute(List<String> parameters) {
        return logout();
    }
    
    private String logout() {
        dealershipRepository.setLoggedUser(null);
        return USER_LOGGED_OUT;
    }
    
}
