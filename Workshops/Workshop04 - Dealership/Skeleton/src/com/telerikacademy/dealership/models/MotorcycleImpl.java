package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    private final static int MIN_CATEGORY=3;
    private final static int MAX_CATEGORY=10;
    private final static String CATEGORY_ERROR_MESSAGE=String.format("Category must be between %d and %d characters long!", MIN_CATEGORY, MAX_CATEGORY);

    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE, VehicleType.MOTORCYCLE.getWheelsFromType());
        Validator.ValidateIntRange(category.length(), MIN_CATEGORY, MAX_CATEGORY, CATEGORY_ERROR_MESSAGE);
        this.category=category;
    }

    @Override
    public String getCategory() {
        return category;
    }


    @Override
    protected String printAdditionalInfo() {
        return String.format(" Category: %s", getCategory());
    }

}
