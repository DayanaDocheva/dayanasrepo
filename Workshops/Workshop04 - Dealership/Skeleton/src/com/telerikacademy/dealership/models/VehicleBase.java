package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD="Make";
    private final static String MODEL_FIELD="Model";
    private final static String PRICE_FIELD="Price";
    private final static String WHEELS_FIELD="Wheels";
    private final static String COMMENTS_HEADER="    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER="    --NO COMMENTS--";
    private final static int MIN_MAKE=2;
    private final static int MAX_MAKE=15;
    private final static String MAKE_NULL="Make cannot be null.";
    private final static String MAKE_ERROR_MESSAGE=String.format("Make must be between %d and %d characters long!", MIN_MAKE, MAX_MAKE);
    private final static int MIN_MODEL=1;
    private final static int MAX_MODEL=15;
    private final static String MODEL_ERROR_MESSAGE=String.format("Model must be between %d and %d characters long!", MIN_MODEL, MAX_MODEL);
    private final static String MODEL_NULL="Model cannot be null";
    private final static double MIN_PRICE=0.0;
    private final static double MAX_PRICE=1000000.0;
    private final static String PRICE_ERROR_MESSAGE=String.format("Price must be between %.1f and %.1f!", MIN_PRICE, MAX_PRICE);
    private final static String VEHICLE_TYPE_NULL="Vehicle type cannot be null.";


    private String make;
    private String model;
    private int wheels;
    private double price;
    private VehicleType vehicleType;
    private List<Comment> comments;


    public VehicleBase(String make, String model, double price, VehicleType vehicleType, int wheels) {
        setMake(make);
        setModel(model);
        setPrice(price);
        setVehicleType(vehicleType);
        wheels=vehicleType.getWheelsFromType();
        comments=new ArrayList<>();
    }

    @Override
    public int getWheels() {
        return wheels;
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<Comment>();
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void removeComment(Comment comment) {
        Validator.ValidateNull(comment, "");
        if (!comments.contains(comment)) {
            throw new IllegalArgumentException();
        }
        comments.remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
        Validator.ValidateNull(comment, "");
        comments.add(comment);
    }

    @Override
    public String toString() {
        StringBuilder builder=new StringBuilder();

        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append(String.format(" %s: %s", MAKE_FIELD, make)).append(System.lineSeparator());
        builder.append(String.format(" %s: %s", MODEL_FIELD, model)).append(System.lineSeparator());
        builder.append(String.format(" %s: %d", WHEELS_FIELD, vehicleType.getWheelsFromType())).append(System.lineSeparator());

        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());

        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }


    private void setMake(String make) {
        Validator.ValidateNull(make, MAKE_NULL);
        Validator.ValidateIntRange(make.length(), MIN_MAKE, MAX_MAKE, MAKE_ERROR_MESSAGE);
        this.make=make;
    }

    private void setModel(String model) {
        Validator.ValidateNull(model, MODEL_NULL);
        Validator.ValidateIntRange(model.length(), MIN_MODEL, MAX_MODEL, MODEL_ERROR_MESSAGE);
        this.model=model;
    }


    private void setPrice(double price) {
        Validator.ValidateDecimalRange(price, MIN_PRICE, MAX_PRICE, PRICE_ERROR_MESSAGE);
        this.price=price;
    }

    private void setVehicleType(VehicleType vehicleType) {
        Validator.ValidateNull(vehicleType, VEHICLE_TYPE_NULL);
        this.vehicleType=vehicleType;
    }


    //todo replace this comment with explanation why this method is protected:
//    so that it can be implemented in the subclasses
    protected abstract String printAdditionalInfo();

    private String printComments() {
        StringBuilder builder=new StringBuilder();

        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());

            for (Comment comment : comments) {
                builder.append(comment.toString());
            }

            builder.append(String.format("%s", COMMENTS_HEADER));
        }

        return builder.toString();
    }

}