package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;

public class CarImpl extends VehicleBase implements Car {

    private final static int MIN_SEATS = 1;
    private final static int MAX_SEATS = 10;
    private final static String SEATS_ERROR_MESSAGE = String.format(" Seats must be between %d and %d!", MIN_SEATS, MAX_SEATS);

    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, VehicleType.CAR, VehicleType.CAR.getWheelsFromType());
        Validator.ValidateIntRange(seats, MIN_SEATS, MAX_SEATS, SEATS_ERROR_MESSAGE);
        this.seats = seats;
    }


    @Override
    public int getSeats() {
        return seats;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("Seats: %s", getSeats());
    }
}
