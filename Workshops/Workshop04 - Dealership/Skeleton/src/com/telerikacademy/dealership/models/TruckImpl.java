package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;


public class TruckImpl extends VehicleBase implements Truck {

    private final static int MIN_WEIGHT_CAPACITY=1;
    private final static int MAX_WEIGHT_CAPACITY=100;
    private final static String WEIGHT_CAPACITY_MESSAGE=String.format("Weight capacity must be between %d and %d!", MIN_WEIGHT_CAPACITY, MAX_WEIGHT_CAPACITY);

    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK, VehicleType.TRUCK.getWheelsFromType());
        Validator.ValidateIntRange(weightCapacity, MIN_WEIGHT_CAPACITY, MAX_WEIGHT_CAPACITY, WEIGHT_CAPACITY_MESSAGE);
        this.weightCapacity=weightCapacity;
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }


    @Override
    protected String printAdditionalInfo() {
        return String.format(" Weight Capacity: %dt", getWeightCapacity());
    }
}
