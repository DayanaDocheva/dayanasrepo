package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {


    private static final String INVALID_PRODUCT_MESSAGE="Product can't be null.";
    private static final String DELETE_PRODUCT_ERROR_MESSAGE="Product not found in category.";
    private static final String NAME_NULL="Name cannot be null.";
    private static final int PRODUCTS_MIN_SIZE=0;

    private String name;
    private List<Product> products;

    public CategoryImpl(String name) {
        setName(name);
        products=new ArrayList<>();
    }

@Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException(NAME_NULL);
        }
        this.name = name;
    }

    public List<Product> getProducts() {
        //todo why are we returning a copy? Replace this comment with explanation!
        return new ArrayList<>(products);
    }

@Override
    public void addProduct(Product product) {
        CheckIfProductIsNull(product);
        products.add(product);
    }

@Override
    public void removeProduct(Product product) {
        CheckIfProductIsNull(product);
        if (!products.contains(product)) {
            throw new IllegalArgumentException(DELETE_PRODUCT_ERROR_MESSAGE);
        }
        products.remove(product);
    }

    private void CheckIfProductIsNull(Product product) {
        if (product == null) {
            throw new IllegalArgumentException(INVALID_PRODUCT_MESSAGE);
        }
    }

    //The engine calls this method to print your category! You should not rename it!
@Override
    public String print() {
        if (products.size() == PRODUCTS_MIN_SIZE) {
            return String.format("#Category: %s%n" +
                    " #No product in this category", name);
        } else {
            StringBuilder sb=new StringBuilder();
            sb.append(String.format("#Category: %s%n", name));

            for (Product product : products) {
                sb.append(product.print());
            }
            return sb.toString();
        }

    }
}
