package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class CreamImpl extends ProductBase implements Cream {

    private static final String SCENT_TYPE_INVALID_MESSAGE="Scent type cannot be null.";

    private ScentType scent;


    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        setScent(scent);
    }

    @Override
    public ScentType getScent() {
        return scent;
    }

    private void setScent(ScentType scent) {
        if (scent == null) {
            throw new IllegalArgumentException(SCENT_TYPE_INVALID_MESSAGE);
        }
        this.scent=scent;
    }


    @Override
    public String print() {
        return String.format("%s%n #Scent: %s%n ===", super.print(), getScent());
    }


}




