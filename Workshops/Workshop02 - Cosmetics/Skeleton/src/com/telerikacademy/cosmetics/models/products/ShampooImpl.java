package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class ShampooImpl extends ProductBase implements Shampoo {

    private static final String MILLILITERS_INVALID_MESSAGE="Milliliters cannot be negative number.";
    private static final int MILLILITERS_MIN_NUMBER=3;
    private static final String USAGE_TYPE_INVALID_MESSAGE="Usage type cannot be null.";


    private int milliliters;
    private UsageType usage;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
        setUsage(usage);
    }

    @Override
    public int getMilliliters() {
        return milliliters;
    }

    private void setMilliliters(int milliliters) {
        if (milliliters < MILLILITERS_MIN_NUMBER) {
            throw new IllegalArgumentException(MILLILITERS_INVALID_MESSAGE);
        }
        this.milliliters = milliliters;
    }


    @Override
    public UsageType getUsage() {
        return usage;
    }

    private void setUsage(UsageType usage) {
        if (usage == null) {
            throw new IllegalArgumentException(USAGE_TYPE_INVALID_MESSAGE);
        }
        this.usage = usage;
    }

    @Override
    public String print() {
        return String.format("%s%n #Milliliters: %s %n #Usage: %s%n ===", super.print(), getMilliliters(), getUsage());
    }
}
