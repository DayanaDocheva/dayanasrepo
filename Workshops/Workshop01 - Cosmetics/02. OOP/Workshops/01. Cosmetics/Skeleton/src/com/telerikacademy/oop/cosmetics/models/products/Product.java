package com.telerikacademy.oop.cosmetics.models.products;

import com.telerikacademy.oop.cosmetics.models.common.GenderType;

public class Product {


    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    public Product(String name, String brand, double price, GenderType gender) {

        setPrice(price);
        setName(name);
        setBrand(brand);
        setGender(gender);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.length() < 3 || name.length() > 10) {
            throw new IllegalArgumentException("Not a valid name!");
        }
        this.name=name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        if (brand == null || brand.length() < 2 || brand.length() > 10) {
            throw new IllegalArgumentException("Not a valid brand!");
        }
        this.brand=brand;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Not a valid price!");
        }
        this.price=price;
    }


    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        if (!(gender == GenderType.MEN || gender == GenderType.WOMAN ||gender == GenderType.UNISEX)) {
            throw new IllegalArgumentException("Not a valid gender!");
        }
        this.gender = gender;
    }

    public String print() {
        return String.format(" #%s %s%n #Price: %.2f%n #Gender: %s", getName(), getBrand(), getPrice(), getGender());

    }
}