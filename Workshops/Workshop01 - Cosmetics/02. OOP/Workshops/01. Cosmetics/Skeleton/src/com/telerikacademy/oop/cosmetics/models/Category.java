package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private String name;
    private List<Product> products;

    public Category(String name) {
        setName(name);
        products=new ArrayList<>();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.length() < 2 || name.length() > 15) {
            throw new IllegalArgumentException();
        }
        this.name=name;
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException();
        }
        products.add(product);
    }

    public void removeProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException("No product in this category!");
        }
        products.remove(product);
    }

    public String print() {
        if (name.length() < 2 || name.length() > 15) {
            return String.format("#Category: %s%n + #No products in this category", getName());
        }
        throw new IllegalArgumentException("Invalid product.");
    }
}
