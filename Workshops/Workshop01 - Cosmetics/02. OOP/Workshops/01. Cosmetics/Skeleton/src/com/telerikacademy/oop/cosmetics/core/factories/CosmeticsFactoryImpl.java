package com.telerikacademy.oop.cosmetics.core.factories;

import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsFactory;
import com.telerikacademy.oop.cosmetics.models.Category;
import com.telerikacademy.oop.cosmetics.models.cart.ShoppingCart;
import com.telerikacademy.oop.cosmetics.models.common.GenderType;
import com.telerikacademy.oop.cosmetics.models.products.Product;

public class CosmeticsFactoryImpl implements CosmeticsFactory {

    public Category createCategory(String name) {
        return new Category(name);
    }

    public Product createProduct(String name, String brand, double price, String gender) {

        GenderType compare=GenderType.MEN;
        int flag=0;

        for (GenderType gt : GenderType.values()) {
            if (gt.name().equalsIgnoreCase(gender)) {
                compare=gt;
                flag++;
            }
        }
        if (flag == 0) {
            throw new IllegalArgumentException("Not a valid gender!");
        }

        Product product=new Product(name, brand, price, compare);
        return product;
    }

    public ShoppingCart createShoppingCart() {
        return new ShoppingCart();

    }
}
