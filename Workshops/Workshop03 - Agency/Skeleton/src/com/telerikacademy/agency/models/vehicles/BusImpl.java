package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    private final static int MIN_PASSENGER_CAPACITY=10;
    private final static int MAX_PASSENGER_CAPACITY=50;
    private final static String PASSENGER_CAPACITY_INVALID_MESSAGE=String.format("A bus cannot have less than %d passengers or more than %d passengers.", MIN_PASSENGER_CAPACITY, MAX_PASSENGER_CAPACITY);


    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setType(VehicleType.LAND);
    }

    @Override
    protected void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_PASSENGER_CAPACITY || passengerCapacity > MAX_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(PASSENGER_CAPACITY_INVALID_MESSAGE);
        }
        super.setPassengerCapacity(passengerCapacity);
    }


    @Override
    public int getPassengerCapacity() {
        return super.getPassengerCapacity();
    }

    @Override
    public double getPricePerKilometer() {
        return super.getPricePerKilometer();
    }

    @Override
    public VehicleType getType() {
        return super.getType();
    }

    @Override
    public String print() {
        return super.print();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public String printClassName() {
        return "Bus";
    }
}
