package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {

    private final boolean hasFreeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer, VehicleType.AIR);
        this.hasFreeFood=hasFreeFood;
        setType(VehicleType.AIR);
    }

    @Override
    public String printClassName() {
        return "Airplane";
    }

    @Override
    public boolean hasFreeFood() {
        return hasFreeFood;
    }

    @Override
    public int getPassengerCapacity() {
        return super.getPassengerCapacity();
    }

    @Override
    public double getPricePerKilometer() {
        return super.getPricePerKilometer();
    }

    @Override
    public VehicleType getType() {
        return super.getType();
    }

    @Override
    public String print() {
        return super.print();
    }

    @Override
    public String toString() {
        return String.format(super.print() + " Has free food: %b%n", hasFreeFood());
    }
}
