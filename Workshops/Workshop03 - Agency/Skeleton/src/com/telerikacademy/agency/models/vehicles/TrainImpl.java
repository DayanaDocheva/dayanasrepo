package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    private final static int MIN_PASSENGER_CAPACITY=30;
    private final static int MAX_PASSENGER_CAPACITY=150;
    private final static String PASSENGER_CAPACITY_INVALID_MESSAGE=String.format("A train cannot have less than %d passengers or more than %d passengers.", MIN_PASSENGER_CAPACITY, MAX_PASSENGER_CAPACITY);
    private final static int MIN_CARTS=1;
    private final static int MAX_CARTS=15;
    private final static String CARTS_INVALID_MESSAGE=String.format("A train cannot have less than %d cart or more than %d carts.", MIN_CARTS, MAX_CARTS);

    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setCarts(carts);
        setType(VehicleType.LAND);
    }

    @Override
    protected void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_PASSENGER_CAPACITY || passengerCapacity > MAX_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(PASSENGER_CAPACITY_INVALID_MESSAGE);
        }
        super.setPassengerCapacity(passengerCapacity);
    }

    @Override
    public int getCarts() {
        return carts;
    }

    protected void setCarts(int carts) {
        if (carts < MIN_CARTS || carts > MAX_CARTS) {
            throw new IllegalArgumentException(CARTS_INVALID_MESSAGE);
        }
        this.carts=carts;
    }

    @Override
    public int getPassengerCapacity() {
        return super.getPassengerCapacity();
    }

    @Override
    public double getPricePerKilometer() {
        return super.getPricePerKilometer();
    }

    @Override
    public VehicleType getType() {
        return super.getType();
    }

    @Override
    public String print() {
        return String.format(super.print() + " Carts amount: %d%n", getCarts());
    }

    @Override
    public String printClassName() {
        return "Train";
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
