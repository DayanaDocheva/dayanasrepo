package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {

    private static final int MIN_PASSENGER_CAPACITY=1;
    private static final int MAX_PASSENGER_CAPACITY=800;
    private static final double MIN_PRICE_PER_KILOMETER=0.10;
    private static final double MAX_PRICE_PER_KILOMETER=2.50;
    private static final String PASSENGER_CAPACITY_INVALID_MESSAGE=String.format("A vehicle with less than %d passenger ot more than %d passengers cannot exist!", MIN_PASSENGER_CAPACITY, MAX_PASSENGER_CAPACITY);
    private static final String PRICE_PER_KILOMETER_INVALID_MESSAGE=String.format("A vehicle with a price per kilometer lower than %f or higher than %f cannot exist!", MIN_PRICE_PER_KILOMETER, MAX_PRICE_PER_KILOMETER);
    private static final String TYPE_CANT_BE_NULL="Vehicle type cannot be null";

    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;


    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setType(type);
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    protected void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_PASSENGER_CAPACITY || passengerCapacity > MAX_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(PASSENGER_CAPACITY_INVALID_MESSAGE);
        }
        this.passengerCapacity=passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    protected void setPricePerKilometer(double pricePerKilometer) {
        if (pricePerKilometer < MIN_PRICE_PER_KILOMETER || pricePerKilometer > MAX_PRICE_PER_KILOMETER) {
            throw new IllegalArgumentException(PRICE_PER_KILOMETER_INVALID_MESSAGE);
        }
        this.pricePerKilometer=pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    protected void setType(VehicleType type) {
        if (type == null) {
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        }
        this.type = type;
    }

    @Override
    public String print() {
        return String.format("%s ---- %n Passenger capacity: %d%n Price per kilometer: %.2f%n Vehicle type: %s%n", printClassName(), getPassengerCapacity(), getPricePerKilometer(), getType());
    }

    public abstract String printClassName();

    @Override
    public String toString() {
        return print();
    }
}