package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    private static final int MIN_ADMINISTRATIVE_COSTS = 0;
    private static final String ADMINISTRATIVE_COSTS_INVALID_MESSAGE ="Administrative costs cannot be a negative number";

    Journey journey;
    double administrativeCosts;

    public TicketImpl(Journey journey, double administrativeCosts) {
        this.journey=journey;
        setAdministrativeCosts(administrativeCosts);
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    private void setAdministrativeCosts(double administrativeCosts) {
        if (administrativeCosts < MIN_ADMINISTRATIVE_COSTS) {
            throw new IllegalArgumentException(ADMINISTRATIVE_COSTS_INVALID_MESSAGE);
        }
        this.administrativeCosts=administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    @Override
    public double calculatePrice() {
        return administrativeCosts * journey.calculateTravelCosts();
    }

    @Override
    public String print() {
        return String.format("Ticket---- %nDestination: %s%nPrice: %.2f%n", journey.getDestination(),calculatePrice());
    }

    @Override
    public String toString() {
        return print();
    }
}



