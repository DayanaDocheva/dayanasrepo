package com.telerikacademy.agency.tests.models;

import com.telerikacademy.agency.models.JourneyImpl;
import com.telerikacademy.agency.models.vehicles.BusImpl;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Journey_Tests {
    
    private Vehicle testVehicle;
    
    @BeforeEach
    public void before() {
        testVehicle = new BusImpl(14, 2);
    }
    
    @Test
    public void constructor_should_throw_when_placeStartLocationLengthLessThanMinimum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new JourneyImpl("Sofi", "Varna", 44, testVehicle));
    }
    
    @Test
    public void constructor_should_throw_when_placeDestinationLengthLessThanMinimum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new JourneyImpl("Sofia", "Varn", 44, testVehicle));
    }
    
    @Test
    public void constructor_should_throw_when_placeStartLocationIsNull() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new JourneyImpl(null, "Varna", 44, testVehicle));
    }
    
    @Test
    public void constructor_should_throw_when_placeDestinationIsNull() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new JourneyImpl("Sofia", null, 44, testVehicle));
    }
    
    @Test
    public void constructor_should_throw_when_placeStartLocationLengthMoreThanMaximum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new JourneyImpl(new String(new char[54]), "Varna", 44, testVehicle));
    }
    
    @Test
    public void constructor_should_throw_when_placeDestinationLengthMoreThanMaximum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new JourneyImpl("Sofia", new String(new char[58]), 44, testVehicle));
    }
    
    @Test
    public void constructor_should_throw_when_distanceLessThanMinimum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new JourneyImpl("Sofia", "Varna", 4, testVehicle));
    }
    
    @Test
    public void constructor_should_throw_when_distanceMoreThanMaximum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new JourneyImpl("Sofia", "Varna", 5001, testVehicle));
    }
    
    @Test
    public void constructor_should_throw_when_vehicleIsNull() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new JourneyImpl("Sofia", "Varna", 50, null));
    }
    
}
