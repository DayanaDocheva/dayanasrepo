package testCases.telerikForum;

import constants.ForumConstants;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.TelerikForumHomePage;
import pages.TelerikForumLoginPage;

import java.util.concurrent.TimeUnit;


public class TelerikForumTopicCreationTests extends ForumConstants {

    private WebDriver driver;
    private WebDriverWait wait;


    @BeforeClass
    public static void classSetUp() {
        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();
    }

    @Before
    public void setUp() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        this.driver = new ChromeDriver(options);

//        FirefoxBinary firefoxBinary = new FirefoxBinary();
//        FirefoxOptions firefoxOptions = new FirefoxOptions();
//        firefoxOptions.setBinary(firefoxBinary);
//        this.driver = new FirefoxDriver(firefoxOptions);

        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);
        this.driver.get(TELERIK_FORUM_HOME_PAGE);
    }

    @Test
    public void telerikStageForumHomePageNavigated_When_ForumOpened() {
        TelerikForumHomePage homePage = new TelerikForumHomePage(driver);
        wait.until(ExpectedConditions.visibilityOf(homePage.getSiteLogo()));

        Assert.assertEquals(NAVIGATE_HOME_PAGE_ERROR_MESSAGE, driver.getCurrentUrl(), TELERIK_FORUM_HOME_PAGE);
    }

    @Test
    public void telerikStageForumLoginPageNavigated_When_stageForumHomePageOpened() {
        TelerikForumHomePage homePage = new TelerikForumHomePage(driver);
        homePage.navigateToForumLoginPage();

        Assert.assertTrue( NAVIGATE_LOGIN_PAGE_ERROR_MESSAGE, driver.getCurrentUrl().contains(TELERIK_FORUM_LOGIN_PAGE));
    }

    @Test
    public void signIn_When_LoggedWithValidUserNameAndPassword() {
        TelerikForumHomePage homePage = new TelerikForumHomePage(driver);
        homePage.navigateToForumLoginPage();

        TelerikForumLoginPage loginPage = new TelerikForumLoginPage(driver);
        loginPage.logIn(EMAIL, PASSWORD);


        Assert.assertTrue(LOG_IN_ERROR_MESSAGE, homePage.getCurrentUser().isDisplayed());
    }

    @Test
    public void createNewTopicAndLogOut_When_userIsLoggedIn() {
        TelerikForumHomePage homePage = new TelerikForumHomePage(driver);
        homePage.navigateToForumLoginPage();

        TelerikForumLoginPage loginPage = new TelerikForumLoginPage(driver);
        loginPage.logIn(EMAIL, PASSWORD);

        homePage.createNewTopic(TOPIC_TITLE, TOPIC_BODY);

        Assert.assertTrue(NEW_TOPIC_ERROR_MESSAGE, driver.getPageSource().contains(TOPIC_TITLE));

        homePage.logOut();

        Assert.assertTrue(LOG_OUT_ERROR_MESSAGE, homePage.getLoginButton().isDisplayed());
    }
    

    @After
    public void tearDown() {
        this.driver.quit();
    }

}
