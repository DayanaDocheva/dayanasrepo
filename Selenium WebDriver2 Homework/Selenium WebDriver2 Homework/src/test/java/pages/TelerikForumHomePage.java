package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;



public class TelerikForumHomePage {

    private WebDriver driver;

    public TelerikForumHomePage(WebDriver webDriver) {
        this.driver=webDriver;
    }

    public WebElement getSiteLogo() { return driver.findElement(By.id("site-logo"));
    }
    public WebElement getLoginButton() {
        return driver.findElement(By.xpath("//button[contains(.,'Log In')]"));
    }

    private WebElement getNewTopicButton() {
        return driver.findElement(By.id("create-topic"));
    }

    private WebElement getTitleField() {
        return driver.findElement(By.id("reply-title"));
    }

    private WebElement getBodyField() {
        return driver.findElement(By.xpath("//textarea[@aria-label='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.'] "));
    }

    private WebElement getCreateTopicButton() {
        return driver.findElement(By.xpath("//button[@aria-label='Create Topic']"));
    }

    public WebElement getCurrentUser() {
        return driver.findElement(By.id("current-user"));
    }

    private WebElement getPreferencesIcon() {
        return driver.findElement(By.className("user-preferences-link"));
    }

    public WebElement getLogOutButton() {
        return driver.findElement(By.xpath("//li[contains(@class,'logout')]//button")); }


    public void navigateToForumLoginPage() {
        getLoginButton().click();
    }

    public void createNewTopic(String topicTitle, String topicBody) {
        getNewTopicButton().click();
        getTitleField().sendKeys(new CharSequence[]{topicTitle});
        getBodyField().sendKeys(new CharSequence[]{topicBody});
        getCreateTopicButton().click();
    }


    public void logOut() {
        getCurrentUser().click();
        getPreferencesIcon().click();
        getLogOutButton().click();
    }
}