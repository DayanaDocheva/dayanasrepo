package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TelerikForumLoginPage {

    private WebDriver driver;

    public TelerikForumLoginPage(WebDriver webDriver) {
        this.driver=webDriver;
    }

    private WebElement getEmailField() { return driver.findElement(By.xpath("//input[@id='Email']")); }
    private WebElement getPasswordField() { return driver.findElement(By.xpath("//input[@id='Password']")); }
    private WebElement getSignInButton() { return driver.findElement(By.id("next")); }


    public void logIn(String email, String password) {
        getEmailField().click();
        getEmailField().sendKeys(new CharSequence[] {email});
        getPasswordField().sendKeys(new CharSequence[]{password});
        getSignInButton().click();
    }
}