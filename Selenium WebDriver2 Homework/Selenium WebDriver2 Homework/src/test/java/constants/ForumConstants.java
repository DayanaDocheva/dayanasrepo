package constants;

public class ForumConstants {

    public static final String TELERIK_FORUM_HOME_PAGE = "https://stage-forum.telerikacademy.com/";
    public static final String TELERIK_FORUM_LOGIN_PAGE = "https://auth.telerikacademy.com/account/login";
    public static final String TOPIC_TITLE = "Test topic creation";
    public static final String TOPIC_BODY = "This is a test";
    public static final String EMAIL = "kravdit@gmail.com";
    public static final String PASSWORD = "buddyGroup3";
    public static final String NAVIGATE_HOME_PAGE_ERROR_MESSAGE = "Forum main page was not displayed.";
    public static final String NAVIGATE_LOGIN_PAGE_ERROR_MESSAGE = "Forum login page was not displayed.";
    public static final String LOG_IN_ERROR_MESSAGE = "The user cannot log in.";
    public static final String NEW_TOPIC_ERROR_MESSAGE = "Topic was not created.";
    public static final String LOG_OUT_ERROR_MESSAGE = "The user is logged in.";
}
