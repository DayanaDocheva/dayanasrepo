|Test Case Title|Priority|Technique|
|---|---|---|
|Topic title is alphabetic|Prio 2|Equivalence Partitioning|
|Topic title is not alphabetic|Prio 2|Equivalence Partitioning|
|Test it accepts special characters|Prio 1|Equivalence Partitioning|
|Test for alphabet is accepted in upper and lower case|Prio 2|Equivalence Partitioning|
|Test able to copy the text from the section for topic|Prio 2|Equivalence Partitioning|
|Test able to paste the text in the section for topic|Prio 2|Equivalence Partitioning|
|Тest it groups the topic with related topics|Prio 3|Equivalence Partitioning|
|Creation of topic title with the allowable number of characters|Prio 1|Equivalence Partitioning|
|Creation of topic title with more than the maximum 50 characters|Prio 1|Equivalence Partitioning|
|Creation of topic title with less than the minimum 5 characters|Prio 1|Equivalence Partitioning|
|Creation of topic title with а blank message|Prio 1|Equivalence Partitioning|
|Creation of comment with the minimum boundary of characters|Prio 1|Boundary Value Analysis|
|Creation of comment with one character more than allowed|Prio 2|Boundary Value Analysis|
|Creation of comment with the maximum boundary of characters|Prio 1|Boundary Value Analysis|
|Creation of comment with one character less than allowed|Prio 2|Boundary Value Analysis|
|Log in with correct username and wrong password|Prio 1|Decision table|
|Log in with correct username and password|Prio 1|Decision table|
|Edit a comment after login in to the system|Prio 2|Decision table|
|Logged in user can post a comment in the forum topics|Prio 2|Decision table|
|Logged in user can like a his comment|Prio 2|Decision table|
|Creation of a new topic title with the allowable number of characters by grouping it with related topics|Prio 2|Pairwise|
|Creation of a new topic title only with alphabets and а description only with special characters|Prio 3|Pairwise|
|Creation of a new topic with overlimitted characters at description and title|Prio 1|Pairwise|
|Creation of a comment with the allowable number of characters and attached file|Prio 2|Pairwise|
|A disliked comment can be liked|Prio 2|State transition|
|A liked comment can be disliked|Prio 2|State transition|
