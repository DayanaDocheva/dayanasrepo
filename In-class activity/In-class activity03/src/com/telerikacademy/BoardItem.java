package com.telerikacademy;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BoardItem {

    private static final int MIN_NAME_LENGTH=5;
    private static final int MAX_NAME_LENGTH=30;
    private static final String TITLE_NULL="Please provide a non-empty name.";
    private static final String TITLE_INVALID_MESSAGE= String.format("Please provide a title with length between %s and %s chars", MIN_NAME_LENGTH, MAX_NAME_LENGTH);
    private static final String DATE_INVALID_MESSAGE="DueDate cannot be in the past.";
    private static final String STATUS_INVALID_MESSAGE = "Status cannot be null.";


    protected final Status initialStatus=Status.OPEN;
    private final Status finalStatus=Status.VERIFIED;

    private String title;
    private LocalDate dueDate;
    private Status status;
    public List<EventLog> eventLogs;


    public BoardItem(String title, LocalDate dueDate, Status open) {
        this.title=title;
        this.dueDate=dueDate;
        this.status=initialStatus;
        eventLogs=new ArrayList<>();

        EventLog log = new EventLog(String.format("Item created: %s", this.viewInfo()));
        eventLogs.add(log);
    }

    protected void revertStatus() {
        if (this.status != initialStatus) {
            status=Status.values()[status.ordinal() - 1];
//            EventLog log = new EventLog(String.format("Status changed from %s to %s", this.status, status));
//            eventLogs.add(log);
        } else {
            EventLog log=new EventLog(String.format("Can't revert, already at %s", getStatus()));
            eventLogs.add(log);
        }
    }

    protected void advanceStatus() {
        if (this.status != finalStatus) {
            status=Status.values()[status.ordinal() + 1];
//            EventLog log = new EventLog(String.format("Status changed from %s to %s", this.status, status));
//            eventLogs.add(log);
        } else {
            EventLog log=new EventLog(String.format("Can't advance, already at %s", getStatus()));
            eventLogs.add(log);
        }
    }


    public String getTitle() {
        return title;
    }

    protected void setTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException(TITLE_NULL);
        }
        if (title.length() < MIN_NAME_LENGTH || title.length() > MAX_NAME_LENGTH) {
            throw new IllegalArgumentException(TITLE_INVALID_MESSAGE);
        }

        EventLog log=new EventLog(String.format("Title changed from %s to %s", this.title, title));
        eventLogs.add(log);
        this.title=title;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    protected void setDueDate(LocalDate dueDate) {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException(DATE_INVALID_MESSAGE);
        }
        EventLog log=new EventLog(String.format("DueDate changed from %s to %s", this.dueDate, dueDate));
        eventLogs.add(log);
        this.dueDate=dueDate;
    }


    public Status getStatus() {
        return status;
    }

    protected void setStatus(Status status) {
        if (status == null) {
            throw new IllegalArgumentException(STATUS_INVALID_MESSAGE);
        }
        EventLog log=new EventLog(String.format("Status changed from %s to %s", this.status, status));
        eventLogs.add(log);
        this.status=status;
    }

    public List<EventLog> getEventLogs() {
        return new ArrayList<>(eventLogs);
    }


    protected String viewInfo() {
        return String.format("'%s', [%s | %s]", title, status, dueDate);
    }


    protected void displayHistory() {
        for (EventLog log : eventLogs) {
            System.out.println(log.viewInfo());
        }
    }
}
