package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private static final String ITEM_INVALID_MESSAGE = "Item already in the list";

    private static List <BoardItem> items = new ArrayList();

    public Board () {
    }

    public List<BoardItem> getItems() {
        return new ArrayList<>(items);
    }


    public static void addItem(BoardItem item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }
        if (items.contains(item)) {
            throw new IllegalArgumentException(ITEM_INVALID_MESSAGE);
        }
        items.add(item);
    }

    public static int totalItems() {
        int count=Board.items.size();
        return count;
    }
}
