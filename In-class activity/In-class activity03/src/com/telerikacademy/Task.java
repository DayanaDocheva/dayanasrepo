package com.telerikacademy;

import java.time.LocalDate;
import java.util.List;

public class Task extends BoardItem{

    private static final int MIN_ASSIGNEE_LENGTH=5;
    private static final int MAX_ASSIGNEE_LENGTH=30;
    private static final String ASSIGNEE_NULL = "Please provide a non-empty assignee";
    private static final String ASSIGNEE_INVALID_MESSAGE = String.format("Please provide an assignee with length between %s and %s chars", MIN_ASSIGNEE_LENGTH, MAX_ASSIGNEE_LENGTH);

    private String assignee;

    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate, Status.TODO);
        setStatus(Status.TODO);
        this.assignee = assignee;
    }

    public String getAssignee() {
        return assignee;
    }

    private void setAssignee(String assignee) {
        if (assignee == null) {
            throw new IllegalArgumentException(ASSIGNEE_NULL);
        }
        if (assignee.length() < MIN_ASSIGNEE_LENGTH || assignee.length() > MAX_ASSIGNEE_LENGTH) {
            throw new IllegalArgumentException(ASSIGNEE_INVALID_MESSAGE);
        }
        EventLog log = new EventLog(String.format("Assignee changed from %s to %s", this.assignee, assignee));
        eventLogs.add(log);
        this.assignee = assignee;
    }

    }

