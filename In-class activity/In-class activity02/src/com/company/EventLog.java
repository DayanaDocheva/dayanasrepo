package com.company;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EventLog {

    private final String description;
    private final LocalDateTime timesTamp;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");


    public EventLog(String description) {
        if (description == null) {
            throw new IllegalArgumentException("Description cannot be null");
        }
        this.description=description;
        this.timesTamp = LocalDateTime.now();
    }

    public String getDescription() {
        return description;
    }

    public String getTimesTamp() {
        return timesTamp.format(formatter);
    }


    public String viewInfo() {
        return String.format("[%s] %s", getTimesTamp(), getDescription());
    }
}
