package com.company;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Board {
    private static List <BoardItem> items = new ArrayList();

    public Board () {
    }

    public List<BoardItem> getItems() {
        return new ArrayList<>(items);
    }


    public static void addItem(BoardItem item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }
        if (items.contains(item)) {
            throw new IllegalArgumentException("Item already in the list");
        }
        items.add(item);
    }

    public static int totalItems() {
        int count=Board.items.size();
        return count;
    }
}
