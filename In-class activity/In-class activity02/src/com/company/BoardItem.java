package com.company;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BoardItem {

    public static final int MIN_NAME_LENGTH=5;
    public static final int MAX_NAME_LENGTH=30;

    private final Status initialStatus=Status.OPEN;
    private final Status finalStatus=Status.VERIFIED;

    private String title;
    private LocalDate dueDate;
    private Status status;
    private final List<EventLog> eventLogs;


    public BoardItem(String title, LocalDate dueDate) {
        this.title=title;
        this.dueDate=dueDate;
        this.status=initialStatus;
        eventLogs=new ArrayList<>();
        EventLog log=new EventLog(String.format("Item created: %s", viewInfo()));
        eventLogs.add(log);
    }

    public void revertStatus() {
        if (this.status != initialStatus) {
            status=Status.values()[status.ordinal() - 1];
        } else {
            EventLog log=new EventLog(String.format("Can't revert, already at %s", getStatus()));
            eventLogs.add(log);
        }
    }

    public void advanceStatus() {
        if (this.status != finalStatus) {
            status=Status.values()[status.ordinal() + 1];
        } else {
            EventLog log=new EventLog(String.format("Can't advance, already at %s", getStatus()));
            eventLogs.add(log);
        }
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException("Please provide a non-empty name");
        }
        if (title.length() < MIN_NAME_LENGTH || title.length() > MAX_NAME_LENGTH) {
            throw new IllegalArgumentException("Please provide a title with length between 5 and 30 chars");
        }

        EventLog log=new EventLog(String.format("Title changed from %s to %s", this.title, title));
        eventLogs.add(log);
        this.title=title;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("DueDate can't be in the past");
        }
        EventLog log=new EventLog(String.format("DueDate changed from %s to %s", this.dueDate, dueDate));
        eventLogs.add(log);
        this.dueDate=dueDate;
    }


    public Status getStatus() {
        return status;
    }

    private void setStatus(Status status) {
        if (status == null) {
            throw new IllegalArgumentException("Invalid status");
        }
        EventLog log=new EventLog(String.format("Status changed from %s to %s", this.status, status));
        eventLogs.add(log);
        this.status=status;
    }

    public List<EventLog> getEventLogs() {
        return new ArrayList<>(eventLogs);
    }


    public String viewInfo() {
        return String.format("'%s', [%s | %s]", title, status, dueDate);
    }


    public void displayHistory() {
        for (EventLog log : eventLogs) {
            System.out.println(log.viewInfo());
        }
    }
}

