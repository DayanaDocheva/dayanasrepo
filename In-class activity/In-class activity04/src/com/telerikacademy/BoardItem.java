package com.telerikacademy;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogRecord;

public abstract class BoardItem {

    private static final int MIN_NAME_LENGTH=5;
    private static final int MAX_NAME_LENGTH=30;
    private static final String TITLE_NULL="Please provide a non-empty name.";
    private static final String TITLE_INVALID_MESSAGE=String.format("Please provide a title with length between %s and %s chars", MIN_NAME_LENGTH, MAX_NAME_LENGTH);
    private static final String DATE_INVALID_MESSAGE="DueDate cannot be in the past.";
    private static final String STATUS_INVALID_MESSAGE="Status cannot be null.";


    protected final Status initialStatus=Status.OPEN;
    protected final Status finalStatus=Status.VERIFIED;
    protected final Status secondStatus=Status.TODO;

    private String title;
    private LocalDate dueDate;
    public Status status;
    public List<EventLog> eventLogs;


    public BoardItem(String title, LocalDate dueDate, Status status) {
        this.title=title;
        this.dueDate=dueDate;
        this.status=initialStatus;
        eventLogs=new ArrayList<>();

        EventLog log=new EventLog(String.format("Item created: %s", this.viewInfo()));
        eventLogs.add(log);
    }

    public abstract void revertStatus();

    public abstract void advanceStatus();


    public String getTitle() {
        return title;
    }

    protected void setTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException(TITLE_NULL);
        }
        if (title.length() < MIN_NAME_LENGTH || title.length() > MAX_NAME_LENGTH) {
            throw new IllegalArgumentException(TITLE_INVALID_MESSAGE);
        }

        EventLog log=new EventLog(String.format("Title changed from %s to %s", this.title, title));
        eventLogs.add(log);
        this.title=title;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    protected void setDueDate(LocalDate dueDate) {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException(DATE_INVALID_MESSAGE);
        }
        EventLog log=new EventLog(String.format("DueDate changed from %s to %s", this.dueDate, dueDate));
        eventLogs.add(log);
        this.dueDate=dueDate;
    }


    public Status getStatus() {
        return status;
    }

    protected void setStatus(Status status) {
        if (status == null) {
            throw new IllegalArgumentException(STATUS_INVALID_MESSAGE);
        }
        this.status=status;
    }

    public List<EventLog> getEventLogs() {
        return new ArrayList<>(eventLogs);
    }


    protected String viewInfo() {
        return String.format("'%s', [%s | %s]", title, status, dueDate);
    }


    public String getHistory() {
        StringBuilder builder = new StringBuilder();

        for (EventLog event : eventLogs) {
            builder.append(event.viewInfo()).append(System.lineSeparator());
        }

        return builder.toString();
    }

}