package com.telerikacademy;

import Logger.ConsoleLogger;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        BoardItem task = new Task("Write unit tests", "Pesho", tomorrow);

        System.out.println(task.viewInfo());



    }
}