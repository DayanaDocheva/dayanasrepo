package com.telerikacademy;

import java.time.LocalDate;

public class Issue extends BoardItem {

    private String description;

    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate, Status.OPEN);
        setDescription(description);
        setStatus(Status.OPEN);
    }

    public String getDescription() {
        return description;
    }

    private void setDescription(String description) {
        if (description == null) {
            this.description = "No description";
        } else {
            this.description = description;
        }

    }

    @Override
    public void revertStatus() {
        if (this.status != initialStatus) {
            status=Status.values()[status.ordinal() - 1];
            EventLog log=new EventLog(String.format("Issue status set to %s", getStatus()));
            eventLogs.add(log);
        } else {
            EventLog log=new EventLog(String.format("Issue status already %s", getStatus()));
            eventLogs.add(log);
        }
    }

    @Override
    public void advanceStatus() {
        if (this.status != finalStatus) {
            status=Status.values()[status.ordinal() + 1];
            EventLog log=new EventLog(String.format("Issue status set to %s", getStatus()));
            eventLogs.add(log);
        } else {
            EventLog log=new EventLog(String.format("Issue status already %s", getStatus()));
            eventLogs.add(log);
        }
    }

    @Override
    public String viewInfo() {
        String baseInfo = super.viewInfo();
        return String.format("Issue: %s, Description: %s", baseInfo, this.getDescription());
    }

    }
